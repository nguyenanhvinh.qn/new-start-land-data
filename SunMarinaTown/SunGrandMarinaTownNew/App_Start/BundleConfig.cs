﻿using System.Web;
using System.Web.Optimization;

namespace SunGrandMarinaTown
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/js/jquery").Include(
            //             "~/Scripts/jquery.js"));

            bundles.Add(new ScriptBundle("~/js/core").Include(
                        "~/Scripts/jquery.js",
                       "~/wp-includes/js/jquery/ui/position.mine899.js",
                       "~/scripts/toastr.js",
                       "~/wp-content/uploads/pum/pum-site-scriptse50b.js"
                       ));


            bundles.Add(new StyleBundle("~/css/base").Include(
                        "~/wp-content/plugins/wp-helper-lite/public/partials/assets/css/mbwph-styled03b.css",
                        "~/wp-includes/css/dist/block-library/style.mind03b.css",
                        "~/wp-content/plugins/estatik/assets/css/custom/front-archive.mind03b.css",
                        "~/css/font-awesome.mind03b.css",
                        "~/css/flaticond03.css",
                        "~/css/lightgallery.css",
                        "~/wp-content/themes/theratio/style.css",
                        "~/wp-content/plugins/elementor/assets/css/frontend-legacy.minccfb.css",
                        "~/wp-content/plugins/elementor/assets/css/frontend.minccfb.css",
                        "~/css/stylesitefontend.css",
                        "~/wp-content/uploads/elementor/css/post-4546b8c5.css",
                        "~/css/post-style.css",
                        "~/css/pum-site-styles.css",
                        "~/wp-content/uploads/elementor/css/post-6f1e8.cs",
                        "~/wp-content/plugins/sticky-buttons/assets/css/style.min53cf.css",
                        "~/wp-content/plugins/sticky-buttons/assets/vendors/fontawesome/css/fontawesome-all.min42f0.css",
                        "~/wp-content/plugins/elementor/assets/lib/animations/animations.minccfb.css",


                        "~/Content/toastr.css"
                      ));

            BundleTable.EnableOptimizations = true;
        }
    }
}

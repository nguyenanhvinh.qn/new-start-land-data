﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace SunGrandMarinaTownNew.Common
{
    public class TelegramHelper
    {
        ////https://api.telegram.org/bot1145556556:AAHGGHPKQoLglTuDsIQDMIti4P2jFKwcnDU/getMe   lấy thông tin của bot
        //public static string apitolebot = "1145556556:AAHGGHPKQoLglTuDsIQDMIti4P2jFKwcnDU";
        //public static string myChanel = "@Gcard_New_Message";
        //public static string destID = "-1001308906694";  // lấy ở link : https://api.telegram.org/bot1145556556:AAHGGHPKQoLglTuDsIQDMIti4P2jFKwcnDU/getUpdates  (có dấu - lấy cả lấy thông tin của chanel)


        static readonly string token = "1145556556:AAHGGHPKQoLglTuDsIQDMIti4P2jFKwcnDU";
        static readonly string chatId = "-1001308906694";

        public static string SendMessage(string message)
        {
            string retval = string.Empty;
            string url = $"https://api.telegram.org/bot{token}/sendMessage?chat_id={chatId}&text={message}";

            using (var webClient = new WebClient())
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                retval = webClient.DownloadString(url);
            }

            return retval;
        }
    }
}
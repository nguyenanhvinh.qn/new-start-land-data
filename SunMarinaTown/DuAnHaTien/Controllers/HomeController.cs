﻿using DuAnHaTien.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DuAnHaTien.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult YeuCauNhanBaoGia(string hoten, string sodienthoai, string sanpham, string noidungtinnhan)
        {
            #region Gửi mail

            string fromEmail = PublicConstant.Send_Email;
            string fromPassword = PublicConstant.Send_Pass;
            string toEmail = PublicConstant.email_nhan;

            string subject = "Yêu cầu nhận báo giá New Vegas: " + sodienthoai + " " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy hh:mm");

            string body = "";
            body += "Tên khách hàng: " + hoten + "\n\n";
            body += "Điện thoại: " + sodienthoai + "\n\n";
            body += "Sản phẩm: " + sanpham + "\n\n";
            body += "Tin nhắn: " + noidungtinnhan + "\n\n";

            TelegramHelper.SendMessage(subject + " " + body);

            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromEmail, fromPassword);
                    smtp.Timeout = 20000;
                }
                smtp.Send(fromEmail, toEmail, subject, body);
            }
            catch (Exception ex)
            {
            }

            TempData["message"] = "Cảm ơn quý khách đã gửi yêu cầu nhận báo giá chúng tôi sẽ gửi báo giá sớm nhất đến quý khách";
            #endregion

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DatLichThamQuan(string hoten, string sodienthoai, string thoigian, string sanpham)
        {
            #region Gửi mail

            string fromEmail = PublicConstant.Send_Email;
            string fromPassword = PublicConstant.Send_Pass;
            string toEmail = PublicConstant.email_nhan;

            string subject = "Đăng ký tham quan dự án New Vegas: " + sodienthoai + " " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy hh:mm");

            string body = "";
            body += "Tên khách hàng: " + hoten + "\n\n";
            body += "Điện thoại: " + sodienthoai + "\n\n";
            body += "Thời gian tham quan: " + thoigian + "\n\n";
            body += "Dự án: " + sanpham + "\n\n";

            TelegramHelper.SendMessage(subject + " " + body);

            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromEmail, fromPassword);
                    smtp.Timeout = 20000;
                }
                smtp.Send(fromEmail, toEmail, subject, body);
            }
            catch (Exception ex)
            {
            }

            TempData["message"] = "Cảm ơn quý khách đã gửi yêu cầu nhận báo giá chúng tôi sẽ gửi báo giá sớm nhất đến quý khách";
            #endregion

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult NhanBaoGiaQuaEmail(string email)
        {
            #region Gửi mail

            string fromEmail = PublicConstant.Send_Email;
            string fromPassword = PublicConstant.Send_Pass;
            string toEmail = PublicConstant.email_nhan;

            string subject = "Yêu cầu nhận báo giá New Vegas qua email: " + email + " " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy hh:mm");

            string body = "";
            body += "Email: " + subject + "\n\n";

            TelegramHelper.SendMessage(subject + " " + body);

            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromEmail, fromPassword);
                    smtp.Timeout = 20000;
                }
                smtp.Send(fromEmail, toEmail, subject, body);
            }
            catch (Exception ex)
            {
            }

            TempData["message"] = "Cảm ơn quý khách đã gửi yêu cầu nhận báo giá chúng tôi sẽ gửi báo giá sớm nhất đến quý khách";
            #endregion

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}